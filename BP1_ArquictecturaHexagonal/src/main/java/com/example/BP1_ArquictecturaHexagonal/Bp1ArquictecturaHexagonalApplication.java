package com.example.BP1_ArquictecturaHexagonal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.swing.*;

@SpringBootApplication
@EnableFeignClients
public class Bp1ArquictecturaHexagonalApplication {

	public static void main(String[] args) {

		SpringApplication.run(Bp1ArquictecturaHexagonalApplication.class, args);
	}

}
