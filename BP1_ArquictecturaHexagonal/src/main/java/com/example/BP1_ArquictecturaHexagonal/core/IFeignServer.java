package com.example.BP1_ArquictecturaHexagonal.core;

import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.ProfesorOutputDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "feign", url = "http://localhost:8081/")
public interface IFeignServer {

    @GetMapping("profesor/{id}")
    ResponseEntity<ProfesorOutputDto> devolverProfeforIDdesdePersona(@PathVariable("id") String id);

}