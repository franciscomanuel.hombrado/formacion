package com.example.BP1_ArquictecturaHexagonal.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class CustomError {
    private Date timestamp;
    private Integer httpCode;
    private String mensaje;
}
