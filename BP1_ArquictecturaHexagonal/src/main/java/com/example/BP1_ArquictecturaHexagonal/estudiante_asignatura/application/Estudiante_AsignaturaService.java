package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.application;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_AsignaturaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_Asignatura_ListadoAsignaturasOutputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_Asignatura_PersonaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.ProfesorOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.Profesor_PersonaOutputDto;

import java.util.List;

public interface Estudiante_AsignaturaService {
    public Estudiante_AsignaturaOutputDto crear(Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto);
    public Estudiante_AsignaturaOutputDto modificar(String id, Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto);
    public void eliminar(String id);

    public Estudiante_AsignaturaOutputDto devolverPorID(String id);
    public List<Estudiante_Asignatura_ListadoAsignaturasOutputDto> devolverPorIDEstudiante(String id);
    //public Estudiante_Asignatura_PersonaOutputDto devolverEstudiante_Asignatura_PersonaPorID(String id);

    public List<Estudiante_AsignaturaOutputDto> getEstudiante_Asignatura();
    //public List<Estudiante_Asignatura_PersonaOutputDto> getEstudiante_Asignatura_Persona();
}
