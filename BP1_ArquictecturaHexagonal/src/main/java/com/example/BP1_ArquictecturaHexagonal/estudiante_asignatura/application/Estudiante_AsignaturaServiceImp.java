package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.application;

import com.example.BP1_ArquictecturaHexagonal.error.NotFoundException;
import com.example.BP1_ArquictecturaHexagonal.error.UnprocesableException;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_AsignaturaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_Asignatura_ListadoAsignaturasOutputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.repository.jpa.Estudiante_AsignaturaRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.repository.jpa.StudentRepositoryJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Estudiante_AsignaturaServiceImp implements Estudiante_AsignaturaService{
    @Autowired
    Estudiante_AsignaturaRepositoryJpa estudiante_asignaturaJpa;

    @Autowired
    StudentRepositoryJpa studentRepositoryJpa;

    @Override
    public Estudiante_AsignaturaOutputDto crear(Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto) {
        Student student = studentRepositoryJpa.findById(estudiante_asignaturaInputDto.getId_student()).orElseThrow(
                () -> new NotFoundException("CREAR - Student con ID *" + estudiante_asignaturaInputDto.getId_student() + "* no encontrado") );
        return new Estudiante_AsignaturaOutputDto(estudiante_asignaturaJpa.save(new Estudiante_Asignatura(estudiante_asignaturaInputDto, student)));
    }

    @Override
    public Estudiante_AsignaturaOutputDto modificar(String id, Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto) {
        Estudiante_Asignatura estudiante_asignatura =  estudiante_asignaturaJpa.findById(id).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Asignatura con ID *" + id + "* no encontrada") );
        Student student = studentRepositoryJpa.findById(estudiante_asignaturaInputDto.getId_student()).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Estudiante con ID *" + estudiante_asignaturaInputDto.getId_student() + "* no encontrada") );
        estudiante_asignatura.setEstudianteAsignatura(estudiante_asignaturaInputDto, student);
        return new Estudiante_AsignaturaOutputDto(estudiante_asignaturaJpa.save(estudiante_asignatura));
    }

    @Override
    public void eliminar(String id) {
        Estudiante_Asignatura estudiante_asignatura = estudiante_asignaturaJpa.findById(id).orElseThrow(
                () -> new NotFoundException("ELIMINAR - Asignatura con id *" + id + "* no encontrado") );

        if(estudiante_asignatura.getStudent() != null){
            throw new UnprocesableException("ELIMINAR - Asignatura *" + estudiante_asignatura.getAsignatura() +
                    "* tiene asignado el Estudiante *" + estudiante_asignatura.getStudent().getId_student());
        }

        estudiante_asignaturaJpa.deleteById(id);
    }

    @Override
    public Estudiante_AsignaturaOutputDto devolverPorID(String id) {
       return new Estudiante_AsignaturaOutputDto(estudiante_asignaturaJpa.findById(id).orElseThrow(
                () -> new NotFoundException("DEVOLVER POR ID - Asignatura con id *" + id + "* no encontrada") ));
    }

    @Override
    public List<Estudiante_Asignatura_ListadoAsignaturasOutputDto> devolverPorIDEstudiante(String id) {
        Student student = studentRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Estudiante con ID *" + id + "* no encontrada") );

        List<Estudiante_Asignatura> estudiante_asignatura = estudiante_asignaturaJpa.findByStudent(student);

        List<Estudiante_Asignatura_ListadoAsignaturasOutputDto> listado = new ArrayList<>();
        for(int i = 0; i < estudiante_asignatura.size(); i++){
            listado.add(new Estudiante_Asignatura_ListadoAsignaturasOutputDto(estudiante_asignatura.get(i)));
        }
        return listado;
    }

    @Override
    public List<Estudiante_AsignaturaOutputDto> getEstudiante_Asignatura() {
        List<Estudiante_Asignatura> estudiante_asignaturas = estudiante_asignaturaJpa.findAll();
        List<Estudiante_AsignaturaOutputDto> estudiante_asignaturaOutputDtos = new ArrayList<>();
        for(Estudiante_Asignatura e: estudiante_asignaturas){
            estudiante_asignaturaOutputDtos.add(new Estudiante_AsignaturaOutputDto(e));
        }
        return  estudiante_asignaturaOutputDtos;
    }
}
