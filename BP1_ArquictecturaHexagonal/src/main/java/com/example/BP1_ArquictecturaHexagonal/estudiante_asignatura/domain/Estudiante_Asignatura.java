package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain;

import com.example.BP1_ArquictecturaHexagonal.error.UnprocesableException;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import com.example.BP1_ArquictecturaHexagonal.util.StringPrefixedSequenceIdGenerator;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.NotActiveException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "estudiante_asignatura")
public class Estudiante_Asignatura {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "asignatura_seq")
    @GenericGenerator(
            name = "asignatura_seq",
            strategy = "com.example.BP1_ArquictecturaHexagonal.util.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "Asignatura"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%01d"),
            }
    )
    @Column(name = "ID_ASIGNATURA")
    private String id_asignatura;

    @ManyToOne
    @JoinColumn(name = "ID_STUDENT")
    private Student student;

    @Column(name = "ASIGNATURA")
    private String asignatura;

    @Column(name = "COMENTARIOS")
    private String comments;

    @Column(name = "FECHA_INICIAL", nullable = false)
    private Date initial_date;

    @Column(name = "FECHA_FIN", nullable = false)
    private Date finish_date;

    public Estudiante_Asignatura(Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto){
        if(estudiante_asignaturaInputDto.getAsignatura() != null) this.asignatura = estudiante_asignaturaInputDto.getAsignatura();
        else throw new UnprocesableException("CREAR - Campo asignatura vacio");
        if(estudiante_asignaturaInputDto.getComments() != null) this.comments = estudiante_asignaturaInputDto.getComments();
        else throw new UnprocesableException("CREAR - Campo comments vacio");
        if(estudiante_asignaturaInputDto.getInitial_date() != null) this.initial_date = estudiante_asignaturaInputDto.getInitial_date();
        else throw new UnprocesableException("CREAR - Campo initial_date vacio");
        if(estudiante_asignaturaInputDto.getFinish_date() != null) this.finish_date = estudiante_asignaturaInputDto.getFinish_date();
        else throw new UnprocesableException("CREAR - Campo finish_date vacio");
    }

    public Estudiante_Asignatura(Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto, Student student){
        this.student = student;
        if(estudiante_asignaturaInputDto.getAsignatura() != null) this.asignatura = estudiante_asignaturaInputDto.getAsignatura();
            else throw new UnprocesableException("CREAR - Campo asignatura vacio");
        if(estudiante_asignaturaInputDto.getComments() != null) this.comments = estudiante_asignaturaInputDto.getComments();
            else throw new UnprocesableException("CREAR - Campo comments vacio");
        if(estudiante_asignaturaInputDto.getInitial_date() != null) this.initial_date = estudiante_asignaturaInputDto.getInitial_date();
            else throw new UnprocesableException("CREAR - Campo initial_date vacio");
        if(estudiante_asignaturaInputDto.getFinish_date() != null) this.finish_date = estudiante_asignaturaInputDto.getFinish_date();
            else throw new UnprocesableException("CREAR - Campo finish_date vacio");
    }

    public void setEstudianteAsignatura(Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto, Student student){
        if(estudiante_asignaturaInputDto.getId_student() != null) this.student = student;
            else throw new UnprocesableException("CREAR - Campo id_student vacio");
        if(estudiante_asignaturaInputDto.getAsignatura() != null) this.asignatura = estudiante_asignaturaInputDto.getAsignatura();
            else throw new UnprocesableException("CREAR - Campo asignatura vacio");
        if(estudiante_asignaturaInputDto.getComments() != null) this.comments = estudiante_asignaturaInputDto.getComments();
            else throw new UnprocesableException("CREAR - Campo comments vacio");
        if(estudiante_asignaturaInputDto.getInitial_date() != null) this.initial_date = estudiante_asignaturaInputDto.getInitial_date();
            else throw new UnprocesableException("CREAR - Campo initial_date vacio");
        if(estudiante_asignaturaInputDto.getFinish_date() != null) this.finish_date = estudiante_asignaturaInputDto.getFinish_date();
            else throw new UnprocesableException("CREAR - Campo finish_date vacio");
    }
}
