package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.application.Estudiante_AsignaturaService;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_AsignaturaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_Asignatura_ListadoAsignaturasOutputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output.Estudiante_Asignatura_PersonaOutputDto;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("estudiante_asignatura")
public class Estudiante_AsignaturaController {
    @Autowired
    Estudiante_AsignaturaService estudiante_asignaturaService;

    @PostMapping
    public Estudiante_AsignaturaOutputDto crearEstudiante_Asignatura(@RequestBody Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto){
        return estudiante_asignaturaService.crear(estudiante_asignaturaInputDto);
    }
    
    @GetMapping("{id}")
    public Estudiante_AsignaturaOutputDto getEstudianteAsignatura(@PathVariable String id, 
                                                                  @DefaultValue("simple")
                                                                  @RequestParam("outputType") String outputType){
        /*
        switch (outputType){
            case "full":
                return estudiante_asignaturaService.devolverPorID(id);
                
            default:
                return estudiante_asignaturaService.devolverEstudiante_Asignatura_PersonaPorID(id);
        }
*/
        return estudiante_asignaturaService.devolverPorID(id);
    }

    @GetMapping("student/{id_student}")
    public List<Estudiante_Asignatura_ListadoAsignaturasOutputDto> getEstudianteAsignaturasPorIDStudent(@PathVariable String id_student){
        return estudiante_asignaturaService.devolverPorIDEstudiante(id_student);
    }
    
    @PutMapping("{id}")
    public Estudiante_AsignaturaOutputDto modificarEstudiante_Asignatura(@PathVariable String id, 
                                                                         @RequestBody Estudiante_AsignaturaInputDto estudiante_asignaturaInputDto){
        return estudiante_asignaturaService.modificar(id, estudiante_asignaturaInputDto);
    }
    
    @DeleteMapping("{id}")
    public void eliminar(@PathVariable String id){
        estudiante_asignaturaService.eliminar(id);
    }
    
    @GetMapping
    public List<Estudiante_AsignaturaOutputDto> getEstudiantes_Asignaturas(@DefaultValue("simple")
                                                                                         @RequestParam("outputType") String outputType){
        /*
        switch (outputType){
            case "full":
                return estudiante_asignaturaService.getEstudiante_Asignatura();

            default:
                return estudiante_asignaturaService.getEstudiante_Asignatura_Persona();
        }
*/
        return estudiante_asignaturaService.getEstudiante_Asignatura();
    }
}
