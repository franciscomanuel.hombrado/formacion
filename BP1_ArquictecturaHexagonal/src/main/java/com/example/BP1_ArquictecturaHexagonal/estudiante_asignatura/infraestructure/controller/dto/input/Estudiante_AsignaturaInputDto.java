package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Estudiante_AsignaturaInputDto {
    private String id_student;
    private String asignatura;
    private String comments;
    private Date initial_date;
    private Date finish_date;
}
