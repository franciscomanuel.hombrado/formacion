package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Estudiante_AsignaturaOutputDto {
    protected String id_asignatura;
    protected String id_student;
    protected String asignatura;
    protected String comments;
    protected Date initial_date;
    protected Date finish_date;

    public Estudiante_AsignaturaOutputDto(Estudiante_Asignatura estudiante_asignatura){
        this.id_asignatura = estudiante_asignatura.getId_asignatura();
        this.id_student = estudiante_asignatura.getStudent().getId_student();
        this.asignatura = estudiante_asignatura.getAsignatura();
        this.comments = estudiante_asignatura.getComments();
        this.initial_date = estudiante_asignatura.getInitial_date();
        this.finish_date = estudiante_asignatura.getFinish_date();
    }
}
