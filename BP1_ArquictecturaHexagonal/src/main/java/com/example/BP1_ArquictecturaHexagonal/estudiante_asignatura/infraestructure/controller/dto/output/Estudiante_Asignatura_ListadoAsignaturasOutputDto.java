package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Estudiante_Asignatura_ListadoAsignaturasOutputDto {
    private String id_student;
    private List<String> id_asignaturas = new ArrayList<>();

    public Estudiante_Asignatura_ListadoAsignaturasOutputDto(Estudiante_Asignatura estudiante_asignatura){
        this.id_student = estudiante_asignatura.getStudent().getId_student();
        for(Estudiante_Asignatura e : estudiante_asignatura.getStudent().getEstudiante_asignaturas()){
            this.id_asignaturas.add(e.getId_asignatura());
        }
    }
}
