package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Estudiante_Asignatura_PersonaOutputDto extends Estudiante_AsignaturaOutputDto{
    private String id_student;
    private Persona persona;
    private Integer num_hours_week;
    private String comment;
    private Profesor profesor;
    private Estudiante_Asignatura estudiante_asignatura;
    private String branch;


}
