package com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.repository.jpa;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface Estudiante_AsignaturaRepositoryJpa extends JpaRepository<Estudiante_Asignatura, String> {
    //@Query(value = "SELECT e FROM Estudiante_Asignatura e WHERE e.id_student = ?1")
    List<Estudiante_Asignatura> findByStudent(Student student);
}
