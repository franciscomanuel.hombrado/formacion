package com.example.BP1_ArquictecturaHexagonal.persona.application;

import com.example.BP1_ArquictecturaHexagonal.error.NotFoundException;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.input.PersonaInputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output.PersonaOutputDto;

import java.util.List;

public interface PersonaService {
    public PersonaOutputDto crear(PersonaInputDto personaInputDto);
    public PersonaOutputDto modificar(int id, PersonaInputDto personaInputDto);
    public void eliminar(int id) throws NotFoundException, Exception;

    public PersonaOutputDto devolverPorID(int id);
    public PersonaOutputDto devolverPorIDFull(int id);
    public List<PersonaOutputDto> devolverPorUsuario(String usuario);

    public List<PersonaOutputDto> getPersonas();
}
