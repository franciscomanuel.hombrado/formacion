package com.example.BP1_ArquictecturaHexagonal.persona.application;

import com.example.BP1_ArquictecturaHexagonal.error.NotFoundException;
import com.example.BP1_ArquictecturaHexagonal.error.UnprocesableException;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.input.PersonaInputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output.PersonaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output.Persona_ProfesorOutputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output.Persona_StudentOutputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.repository.jpa.PersonaRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.repository.jpa.ProfesorRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.repository.jpa.StudentRepositoryJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PersonaServiceImp implements PersonaService{
    @Autowired
    PersonaRepositoryJpa personaRepositoryJpa;

    @Autowired
    StudentRepositoryJpa studentRepositoryJpa;

    @Autowired
    ProfesorRepositoryJpa profesorRepositoryJpa;

    @Override
    public PersonaOutputDto crear(PersonaInputDto personaInputDto) {
        Persona personaEntity = new Persona(personaInputDto);
        Persona personaDevuelta = personaRepositoryJpa.save(personaEntity);

        return new PersonaOutputDto(personaDevuelta);
    }

    @Override
    public PersonaOutputDto modificar(int id, PersonaInputDto personaInputDto){
        Persona personaEncontrar = null;
        try {
            personaEncontrar = personaRepositoryJpa.findById(id).get();

            personaEncontrar.setPersona(id, personaInputDto);
            Persona persona = personaRepositoryJpa.save(personaEncontrar);
            return new PersonaOutputDto(persona);
        }catch (NoSuchElementException k){
            throw new NotFoundException("MODIFICAR - Persona con id " + id + " no encontrada");
        }
    }

    @Override
    public void eliminar(int id) throws Exception {
        Persona persona = personaRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("ELIMINAR - Persona con id " + id + " no encontrada") );


        if(persona.getStudent() != null){
            throw new UnprocesableException("ELIMINAR - Persona *" + persona.getId_persona() + "* es el Estudiante *" + persona.getStudent().getId_student());
        }

        if(persona.getProfesor() != null){
            throw new UnprocesableException("ELIMINAR - Persona *" + persona.getId_persona() + "* es el Profesor *" + persona.getProfesor().getId_profesor());
        }

        personaRepositoryJpa.deleteById(id);
    }

    @Override
    public PersonaOutputDto devolverPorID(int id) {
        Persona persona = personaRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("DEVOLVER POR ID - Persona con id " + id + " no encontrada") );

        return new PersonaOutputDto(persona);
    }

    @Override
    public PersonaOutputDto devolverPorIDFull(int id) {
        Persona persona = personaRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("DEVOLVER POR ID - Persona con id " + id + " no encontrada") );

        if(persona.getStudent() != null){
            return new Persona_StudentOutputDto(persona);
        }else if(persona.getProfesor() != null){
            return new Persona_ProfesorOutputDto(persona);
        }else{
            return new PersonaOutputDto(persona);
        }
    }


    @Override
    public List<PersonaOutputDto> devolverPorUsuario(String usuario) {
        List<Persona> personas = personaRepositoryJpa.findByUsuario(usuario);
        List<PersonaOutputDto> personaOutputDtos = new ArrayList<>();
        for(Persona p: personas){
            personaOutputDtos.add(new PersonaOutputDto(p));
        }
        return personaOutputDtos;
    }

    @Override
    public List<PersonaOutputDto> getPersonas() {
        List<Persona> personas = personaRepositoryJpa.findAll();
        List<PersonaOutputDto> personaOutputDtos = new ArrayList<>();
        for(Persona p: personas){
            personaOutputDtos.add(new PersonaOutputDto(p));
        }
        return personaOutputDtos;
    }
}
