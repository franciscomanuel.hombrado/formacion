package com.example.BP1_ArquictecturaHexagonal.persona.domain;

import com.example.BP1_ArquictecturaHexagonal.error.UnprocesableException;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.input.PersonaInputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Setter
@Getter
@Entity
public class Persona {
    @Id
    @GeneratedValue
    private int id_persona;

    @Column(name = "USUARIO", nullable = false, length = 10)
    private String usuario;

    @Column(name = "CONTRASEÑA", nullable = false)
    private String password;

    @Column(name = "NOMBRE", nullable = false)
    private String name;

    @Column(name = "APELLIDO")
    private String surname;

    @Column(name = "EMAIL_COMPAÑIA", nullable = false)
    private String company_email;

    @Column(name = "EMAIL_PERSONAL", nullable = false)
    private String personal_email;

    @Column(name = "CIUDAD", nullable = false)
    private String city;

    @Column(name = "ACTIVO", nullable = false)
    private Boolean active;

    @Column(name = "FECHA", nullable = false)
    private Date created_date;

    @Column(name = "ULR_IMAGEN")
    private String imagen_url;

    @Column(name = "FECHA_TERMINACION")
    private Date termination_date;

    @OneToOne(mappedBy = "persona", cascade = CascadeType.ALL, orphanRemoval = true)
    private Student student;

    @OneToOne(mappedBy = "persona", cascade = CascadeType.ALL, orphanRemoval = true)
    private Profesor profesor;

    public Persona(PersonaInputDto personaInputDto){
        if(personaInputDto.getUsuario() != null) this.usuario = personaInputDto.getUsuario();
            else throw new UnprocesableException("CREAR - Campo usuario vacio");
        if(personaInputDto.getPassword() != null) this.password = personaInputDto.getPassword();
            else throw new UnprocesableException("CREAR - Campo password vacio");
        if(personaInputDto.getName() != null) this.name = personaInputDto.getName();
            else throw new UnprocesableException("CREAR - Campo nombre vacio");
        if(personaInputDto.getSurname() != null) this.surname = personaInputDto.getSurname();
            else throw new UnprocesableException("CREAR - Campo surname vacio");
        if(personaInputDto.getCompany_email() != null) this.company_email = personaInputDto.getCompany_email();
            else throw new UnprocesableException("CREAR - Campo company_email vacio");
        if(personaInputDto.getPersonal_email() != null) this.personal_email = personaInputDto.getPersonal_email();
            else throw new UnprocesableException("CREAR - Campo personal_email vacio");
        if(personaInputDto.getCity() != null) this.city = personaInputDto.getCity();
            else throw new UnprocesableException("CREAR - Campo city vacio");
        if(personaInputDto.getActive() != null) this.active = personaInputDto.getActive();
            else throw new UnprocesableException("CREAR - Campo active vacio");
        if(personaInputDto.getCreated_date() != null) this.created_date = personaInputDto.getCreated_date();
            else throw new UnprocesableException("CREAR - Campo created_date vacio");
        if(personaInputDto.getImagen_url() != null) this.imagen_url = personaInputDto.getImagen_url();
            else throw new UnprocesableException("CREAR - Campo imagen_url vacio");
        if(personaInputDto.getTermination_date() != null) this.termination_date = personaInputDto.getTermination_date();
            else throw new UnprocesableException("CREAR - Campo termination_date vacio");
    }

    public void setPersona(int id, PersonaInputDto personaInputDto){
        if(personaInputDto.getUsuario() != null) this.usuario = personaInputDto.getUsuario();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo usuario vacio");
        if(personaInputDto.getPassword() != null) this.password = personaInputDto.getPassword();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo password vacio");
        if(personaInputDto.getName() != null) this.name = personaInputDto.getName();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo nombre vacio");
        if(personaInputDto.getSurname() != null) this.surname = personaInputDto.getSurname();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo surname vacio");
        if(personaInputDto.getCompany_email() != null) this.company_email = personaInputDto.getCompany_email();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo company_email vacio");
        if(personaInputDto.getPersonal_email() != null) this.personal_email = personaInputDto.getPersonal_email();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo personal_email vacio");
        if(personaInputDto.getCity() != null) this.city = personaInputDto.getCity();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo city vacio");
        if(personaInputDto.getActive() != null) this.active = personaInputDto.getActive();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo active vacio");
        if(personaInputDto.getCreated_date() != null) this.created_date = personaInputDto.getCreated_date();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo created_date vacio");
        if(personaInputDto.getImagen_url() != null) this.imagen_url = personaInputDto.getImagen_url();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo imagen_url vacio");
        if(personaInputDto.getTermination_date() != null) this.termination_date = personaInputDto.getTermination_date();
            else throw new UnprocesableException("Imposible MODIFICAR persona con ID " + id + ": campo termination_date vacio");
    }
}