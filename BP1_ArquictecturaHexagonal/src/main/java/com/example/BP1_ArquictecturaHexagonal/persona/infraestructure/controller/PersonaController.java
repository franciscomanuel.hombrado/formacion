package com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller;

import com.example.BP1_ArquictecturaHexagonal.core.IFeignServer;
import com.example.BP1_ArquictecturaHexagonal.persona.application.PersonaService;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.input.PersonaInputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output.PersonaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.ProfesorOutputDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("persona")
public class PersonaController {
    @Autowired
    private PersonaService personaService;

    @Autowired
    private IFeignServer iFeignServer;

    @PostMapping
    public PersonaOutputDto crearPersona(@RequestBody PersonaInputDto personaInputDto) {
        return personaService.crear(personaInputDto);
    }

    @GetMapping("{id}")
    public PersonaOutputDto buscarPersonaID(@PathVariable int id/*, @DefaultValue("simple")
                                                                    @RequestParam(value = "outputType") String outputType*/){
        return personaService.devolverPorID(id);
        /*
        switch (outputType){
            case "full":
                return personaService.devolverPorIDFull(id);
            default:
                return personaService.devolverPorID(id);
        }*/
    }

    @GetMapping("/profesor/{id}")
    public ResponseEntity<ProfesorOutputDto> buscarProfesorID(@PathVariable String id){
        return iFeignServer.devolverProfeforIDdesdePersona(id);
        /*HECHO CON RestTemplate
        ResponseEntity<ProfesorOutputDto> response = new RestTemplate().getForEntity(
                "http://localhost:8080/profesor/"+id, ProfesorOutputDto.class);
        return ResponseEntity.ok(response.getBody());*/

    }

    @GetMapping("usuario/{usuario}")
    public List<PersonaOutputDto> buscarPersonaUsuario(@PathVariable String usuario){
        return personaService.devolverPorUsuario(usuario);
    }

    @PutMapping("{id}")
    public PersonaOutputDto modificarPersona(@PathVariable int id, @RequestBody PersonaInputDto personaInputDto) {
        return personaService.modificar(id, personaInputDto);
    }

    @DeleteMapping("{id}")
    public void eliminarPersona(@PathVariable int id) throws Exception {
        personaService.eliminar(id);
    }

    @GetMapping
    public List<PersonaOutputDto> getRegistros(){
        return personaService.getPersonas();
    }
}
