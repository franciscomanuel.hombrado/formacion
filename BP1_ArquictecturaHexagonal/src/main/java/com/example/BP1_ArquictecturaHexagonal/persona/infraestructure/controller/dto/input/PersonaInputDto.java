package com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.input;

import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output.PersonaOutputDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PersonaInputDto {
    private String usuario;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private Boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;
}
