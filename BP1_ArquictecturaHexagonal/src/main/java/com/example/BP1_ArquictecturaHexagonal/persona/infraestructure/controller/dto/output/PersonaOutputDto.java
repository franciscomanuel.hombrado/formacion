package com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PersonaOutputDto {
    protected int id_persona;
    protected String usuario;
    protected String password;
    protected String name;
    protected String surname;
    protected String company_email;
    protected String personal_email;
    protected String city;
    protected Boolean active;
    protected Date created_date;
    protected String imagen_url;
    protected Date termination_date;

    //crear constructor que reciba una Persona
    public PersonaOutputDto(Persona persona){
        this.id_persona = persona.getId_persona();
        this.usuario = persona.getUsuario();
        this.password = persona.getPassword();
        this.name = persona.getName();
        this.surname = persona.getSurname();
        this.company_email = persona.getCompany_email();
        this.personal_email = persona.getPersonal_email();
        this.city = persona.getCity();
        this.active = persona.getActive();
        this.created_date = persona.getCreated_date();
        this.imagen_url = persona.getImagen_url();
        this.termination_date = persona.getTermination_date();
    }

}
