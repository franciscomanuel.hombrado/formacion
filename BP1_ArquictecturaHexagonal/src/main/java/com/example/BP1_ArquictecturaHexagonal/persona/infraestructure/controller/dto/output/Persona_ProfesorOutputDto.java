package com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Persona_ProfesorOutputDto extends PersonaOutputDto{
    private String id_profesor;
    private String comments;
    private String branch;
    private List<String> id_students = new ArrayList<>();

    public Persona_ProfesorOutputDto(Persona persona){
        this.id_persona = persona.getId_persona();
        this.usuario = persona.getUsuario();
        this.password = persona.getPassword();
        this.name = persona.getName();
        this.surname = persona.getSurname();
        this.company_email = persona.getCompany_email();
        this.personal_email = persona.getPersonal_email();
        this.city = persona.getCity();
        this.active = persona.getActive();
        this.created_date = persona.getCreated_date();
        this.imagen_url = persona.getImagen_url();
        this.termination_date = persona.getTermination_date();

        this.id_profesor = persona.getProfesor().getId_profesor();
        this.comments = persona.getProfesor().getComments();
        this.branch = persona.getProfesor().getBranch();
        for(Student s : persona.getProfesor().getStudents()){
            this.id_students.add(s.getId_student());
        }
    }
}
