package com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Persona_StudentOutputDto extends PersonaOutputDto{
    private String id_student;
    private Integer num_hours_week;
    private String comment;
    private String id_profesor;
    private List<Estudiante_Asignatura> estudiante_asignatura = new ArrayList<>();
    private String branch;

    public Persona_StudentOutputDto(Persona persona){
        this.id_persona = persona.getId_persona();
        this.usuario = persona.getUsuario();
        this.password = persona.getPassword();
        this.name = persona.getName();
        this.surname = persona.getSurname();
        this.company_email = persona.getCompany_email();
        this.personal_email = persona.getPersonal_email();
        this.city = persona.getCity();
        this.active = persona.getActive();
        this.created_date = persona.getCreated_date();
        this.imagen_url = persona.getImagen_url();
        this.termination_date = persona.getTermination_date();

        this.id_student = persona.getStudent().getId_student();
        this.num_hours_week = persona.getStudent().getNum_hours_week();
        this.comment = persona.getStudent().getComment();
        this.id_profesor = persona.getStudent().getProfesor().getId_profesor();
        for(Estudiante_Asignatura e: persona.getStudent().getEstudiante_asignaturas()){
            this.estudiante_asignatura.add(e);
        }
        this.branch = persona.getStudent().getBranch();
    }
}
