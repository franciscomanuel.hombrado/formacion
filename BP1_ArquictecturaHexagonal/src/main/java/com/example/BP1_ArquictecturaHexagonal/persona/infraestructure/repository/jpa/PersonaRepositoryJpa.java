package com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.repository.jpa;

import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonaRepositoryJpa extends JpaRepository<Persona, Integer> {
    List<Persona> findByUsuario(String usuario);

    /*
    @Query(value = "SELECT * FROM Persona p WHERE nombre = :nombre")
    List<Persona> buscaPorNombre(String nombre);
    */
}
