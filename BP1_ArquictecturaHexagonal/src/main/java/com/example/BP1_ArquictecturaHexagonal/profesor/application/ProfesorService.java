package com.example.BP1_ArquictecturaHexagonal.profesor.application;

import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.ProfesorOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.Profesor_PersonaOutputDto;

import java.util.List;

public interface ProfesorService {
    public ProfesorOutputDto crear(ProfesorInputDto profesorInputDto) throws Exception;
    public ProfesorOutputDto modificar(String id, ProfesorInputDto profesorInputDto);
    public void eliminar(String id);

    public ProfesorOutputDto devolverPorID(String id);
    public Profesor_PersonaOutputDto devolverPersonaProfesorPorID(String id);

    public List<ProfesorOutputDto> getProfesores();
    public List<Profesor_PersonaOutputDto> getProfesores_Persona();
}
