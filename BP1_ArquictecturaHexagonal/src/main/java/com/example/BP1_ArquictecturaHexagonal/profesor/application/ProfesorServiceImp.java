package com.example.BP1_ArquictecturaHexagonal.profesor.application;

import com.example.BP1_ArquictecturaHexagonal.error.NotFoundException;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.repository.jpa.PersonaRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.ProfesorOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.Profesor_PersonaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.repository.jpa.ProfesorRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfesorServiceImp implements ProfesorService{
    @Autowired
    ProfesorRepositoryJpa profesorRepositoryJpa;

    @Autowired
    PersonaRepositoryJpa personaRepositoryJpa;

    @Override
    public ProfesorOutputDto crear(ProfesorInputDto profesorInputDto) throws NotFoundException {
        Persona personaRef = personaRepositoryJpa.findById(profesorInputDto.getId_persona()).orElseThrow(
                () -> new NotFoundException("CREAR - Persona con ID *" + profesorInputDto.getId_persona() + "* no encontrada"));

        if(personaRef.getStudent() != null){
            throw new NotFoundException("CREAR - Persona con id *" + profesorInputDto.getId_persona() + "* es un Student");
        }

        if(personaRef.getProfesor() != null){
            throw new NotFoundException("CREAR - Persona con id *" + profesorInputDto.getId_persona() + "* es un Profesor");
        }

        return new ProfesorOutputDto(profesorRepositoryJpa.save(new Profesor(profesorInputDto, personaRef)));
    }

    @Override
    public ProfesorOutputDto modificar(String id, ProfesorInputDto profesorInputDto) throws NotFoundException{
        Profesor profesor = profesorRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Profesor con ID *" + id + "* no encontrado"));

        Persona persona = personaRepositoryJpa.findById(profesorInputDto.getId_persona()).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Persona con ID *" + profesorInputDto.getId_persona() + "* no encontrada"));

        profesor.setProfesor(id, profesorInputDto, persona);
        return new ProfesorOutputDto(profesorRepositoryJpa.save(profesor));
    }

    @Override
    public void eliminar(String id) throws NotFoundException {
        profesorRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("ELIMINAR - Profesor con id *" + id + "* no encontrado") );
        profesorRepositoryJpa.deleteById(id);
    }

    @Override
    public ProfesorOutputDto devolverPorID(String id) throws NotFoundException{
        return new ProfesorOutputDto(profesorRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("DEVOLVER POR ID - Profesor con id *" + id + "* no encontrado")));
    }

    @Override
    public Profesor_PersonaOutputDto devolverPersonaProfesorPorID(String id) {
        return new Profesor_PersonaOutputDto(profesorRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("DEVOLVER POR ID - Profesor con id *" + id + "* no encontrado") ));
    }

    @Override
    public List<ProfesorOutputDto> getProfesores() {
        List<Profesor> profesores = profesorRepositoryJpa.findAll();
        List<ProfesorOutputDto> profesoresOutput = new ArrayList<>();
        for(Profesor p : profesores){
            profesoresOutput.add(new ProfesorOutputDto(p));
        }
        return profesoresOutput;
    }

    @Override
    public List<Profesor_PersonaOutputDto> getProfesores_Persona() {
        List<Profesor> profesores = profesorRepositoryJpa.findAll();
        List<Profesor_PersonaOutputDto> profesores_personaOutput = new ArrayList<>();
        for(Profesor p : profesores){
            profesores_personaOutput.add(new Profesor_PersonaOutputDto(p));
        }
        return profesores_personaOutput;
    }
}
