package com.example.BP1_ArquictecturaHexagonal.profesor.domain;

import com.example.BP1_ArquictecturaHexagonal.error.UnprocesableException;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.StudentInputDto;
import com.example.BP1_ArquictecturaHexagonal.util.StringPrefixedSequenceIdGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
@Entity
public class Profesor {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profesor_seq")
    @GenericGenerator(
            name = "profesor_seq",
            strategy = "com.example.BP1_ArquictecturaHexagonal.util.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "Profesor"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%01d"),
            }
    )
    @Column(name = "ID_PROFESOR")
    private String id_profesor;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PERSONA")
    private Persona persona;

    @Column(name = "COMENTARIOS")
    private String comments;

    @Column(name = "RAMA", nullable = false)
    private String branch;

    @OneToMany(mappedBy = "profesor", cascade = CascadeType.ALL)
    private List<Student> students = new ArrayList<>();

    public Profesor(ProfesorInputDto profesorInputDto, Persona persona){
        this.persona = persona;
        if(profesorInputDto.getComments() != null) this.comments = profesorInputDto.getComments();
        else throw new UnprocesableException("CREAR - Campo comments vacio");
        if(profesorInputDto.getBranch() != null) this.branch = profesorInputDto.getBranch();
            else throw new UnprocesableException("CREAR - Campo branch vacio");
    }

    public void setProfesor(String id, ProfesorInputDto profesorInputDto, Persona persona){
        if(profesorInputDto.getId_persona() != null) this.persona = persona;
        else throw new UnprocesableException("Imposible MODIFICAR Profesor con ID " + id + ": campo id_persona vacio");
        if(profesorInputDto.getComments() != null) this.comments = profesorInputDto.getComments();
        else throw new UnprocesableException("Imposible MODIFICAR Profesor con ID " + id + ": campo comments vacio");
        if(profesorInputDto.getBranch() != null) this.branch = profesorInputDto.getBranch();
        else throw new UnprocesableException("Imposible MODIFICAR Profesor con ID " + id + ": campo branch vacio");

    }
}
