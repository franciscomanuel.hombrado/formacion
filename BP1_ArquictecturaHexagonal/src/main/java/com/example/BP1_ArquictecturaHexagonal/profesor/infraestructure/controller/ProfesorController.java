package com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller;

import com.example.BP1_ArquictecturaHexagonal.profesor.application.ProfesorService;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.input.ProfesorInputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.ProfesorOutputDto;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output.Profesor_PersonaOutputDto;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("profesor")
public class ProfesorController {
    @Autowired
    ProfesorService profesorService;

    @PostMapping
    public ProfesorOutputDto crearProfesor(@RequestBody ProfesorInputDto profesorInputDto) throws Exception {
        return profesorService.crear(profesorInputDto);
    }

    @GetMapping("{id}")
    public ProfesorOutputDto buscarProfesor(@PathVariable String id/*, @DefaultValue("simple") @RequestParam(value = "outputType") String outputType*/ ){
        /*
        switch (outputType){
            case "full":
                return profesorService.devolverPersonaProfesorPorID(id);

            default:
                return profesorService.devolverPorID(id);

        }*/
        return profesorService.devolverPorID(id);
    }

    @PutMapping("{id}")
    public ProfesorOutputDto modificarProfesor(@PathVariable String id, @RequestBody ProfesorInputDto profesorInputDto){
        return profesorService.modificar(id, profesorInputDto);
    }

    @DeleteMapping("{id}")
    public void eliminarProfesor(@PathVariable String id){
        profesorService.eliminar(id);
    }
    
    @GetMapping
    public List<? extends ProfesorOutputDto> getProfesores(@DefaultValue("simple") @RequestParam(value = "outputType") String outputType){
        switch (outputType){
            case "full":
                return profesorService.getProfesores_Persona();
                
            default:
                return profesorService.getProfesores();
        }
    }
}
