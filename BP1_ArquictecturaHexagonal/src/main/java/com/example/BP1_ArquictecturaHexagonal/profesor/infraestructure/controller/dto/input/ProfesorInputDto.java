package com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProfesorInputDto {
    private Integer id_persona;
    private String comments;
    private String branch;
}
