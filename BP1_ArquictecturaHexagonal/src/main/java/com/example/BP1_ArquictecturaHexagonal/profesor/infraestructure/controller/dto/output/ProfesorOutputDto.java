package com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProfesorOutputDto {
    protected String id_profesor;
    protected int id_persona;
    protected String comments;
    protected String branch;

    public ProfesorOutputDto(Profesor profesor){
        this.id_profesor = profesor.getId_profesor();
        this.id_persona = profesor.getPersona().getId_persona();
        this.comments = profesor.getComments();
        this.branch = profesor.getBranch();
    }
}
