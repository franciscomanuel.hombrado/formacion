package com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import lombok.Getter;

import java.util.Date;

@Getter
public class Profesor_PersonaOutputDto extends ProfesorOutputDto{
    //Atributos clase Persona
    private int id_persona;
    private String usuario;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private Boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;

    public Profesor_PersonaOutputDto(Profesor profesor){
        this.id_profesor = profesor.getId_profesor();
        this.id_persona = profesor.getPersona().getId_persona();
        this.comments = profesor.getComments();
        this.branch = profesor.getBranch();

        this.id_persona = profesor.getPersona().getId_persona();
        this.usuario = profesor.getPersona().getUsuario();
        this.password = profesor.getPersona().getPassword();
        this.name = profesor.getPersona().getName();
        this.surname = profesor.getPersona().getSurname();
        this.company_email = profesor.getPersona().getCompany_email();
        this.personal_email = profesor.getPersona().getPersonal_email();
        this.city = profesor.getPersona().getCity();
        this.active = profesor.getPersona().getActive();
        this.created_date = profesor.getPersona().getCreated_date();
        this.imagen_url = profesor.getPersona().getImagen_url();
        this.termination_date = profesor.getPersona().getTermination_date();
    }
}
