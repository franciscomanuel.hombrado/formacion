package com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.repository.jpa;

import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfesorRepositoryJpa extends JpaRepository<Profesor, String> {
    List<Profesor> findByPersona(Persona persona);
}
