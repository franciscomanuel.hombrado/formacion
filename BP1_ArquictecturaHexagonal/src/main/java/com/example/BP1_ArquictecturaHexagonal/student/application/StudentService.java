package com.example.BP1_ArquictecturaHexagonal.student.application;

import com.example.BP1_ArquictecturaHexagonal.error.NotFoundException;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.StudentInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.Student_AsignaturasInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output.Student_PersonaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output.StudentOutputDto;

import java.util.List;

public interface StudentService {
    public StudentOutputDto crear(StudentInputDto studentInputDto) throws Exception;
    public StudentOutputDto modificar(String id, StudentInputDto studentInputDto);
    public StudentOutputDto asignarAsignaturas(String id, Student_AsignaturasInputDto student_asignaturasInputDto);
    public StudentOutputDto designarAsignaturas(String id, Student_AsignaturasInputDto student_asignaturasInputDto);
    public void eliminar(String id) throws NotFoundException;

    public StudentOutputDto devolverPorID(String id);
    public Student_PersonaOutputDto devolverPersonaStudentPorID(String id);

    public List<StudentOutputDto> getStudents();
}
