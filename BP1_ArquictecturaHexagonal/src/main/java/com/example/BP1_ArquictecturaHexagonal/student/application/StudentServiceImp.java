package com.example.BP1_ArquictecturaHexagonal.student.application;

import com.example.BP1_ArquictecturaHexagonal.error.NotFoundException;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.repository.jpa.Estudiante_AsignaturaRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.profesor.infraestructure.repository.jpa.ProfesorRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.StudentInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.Student_AsignaturasInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output.Student_PersonaOutputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output.StudentOutputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.infraestructure.repository.jpa.PersonaRepositoryJpa;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.repository.jpa.StudentRepositoryJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StudentServiceImp implements StudentService {
    @Autowired
    StudentRepositoryJpa studentRepositoryJpa;

    @Autowired
    PersonaRepositoryJpa personaRepositoryJpa;

    @Autowired
    ProfesorRepositoryJpa profesorRepositoryJpa;

    @Autowired
    Estudiante_AsignaturaRepositoryJpa estudiante_asignaturaRepositoryJpa;

    @Override
    public StudentOutputDto crear(StudentInputDto studentInputDto) throws NotFoundException {
        Persona personaRef = personaRepositoryJpa.findById(studentInputDto.getId_persona()).orElseThrow(
                () -> new NotFoundException("CREAR - Persona con ID *" + studentInputDto.getId_persona() + "* no encontrada") );

        if(personaRef.getStudent() != null){
            throw new NotFoundException("CREAR - Persona con id *" + studentInputDto.getId_persona() + "* es un Student");
        }

        if(personaRef.getProfesor() != null){
            throw new NotFoundException("CREAR - Persona con id *" + studentInputDto.getId_persona() + "* es un Profesor");
        }

        Profesor profesor = profesorRepositoryJpa.findById(studentInputDto.getId_profesor()).orElseThrow(
                () -> new NotFoundException("CREAR - Profesor con ID *" + studentInputDto.getId_profesor() + "* no encontrado") );

        return new StudentOutputDto(studentRepositoryJpa.save(new Student(studentInputDto, personaRef, profesor)));
    }

    @Override
    public StudentOutputDto modificar(String id, StudentInputDto studentInputDto) throws NotFoundException{
        Student studentEncontrar = studentRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Student con ID *" + id + "* no encontrada"));

        Persona personaRef = personaRepositoryJpa.findById(studentInputDto.getId_persona()).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Persona con ID *" + studentInputDto.getId_persona() + "* no encontrada"));

        studentEncontrar.setStudent(id, studentInputDto, personaRef);
        Student student = studentRepositoryJpa.save(studentEncontrar);
        return new StudentOutputDto(student);
    }

    @Override
    public StudentOutputDto asignarAsignaturas(String id, Student_AsignaturasInputDto student_asignaturasInputDto) {
        Student student = studentRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Student con ID *" + id + "* no encontrada"));

        for(String id_asig : student_asignaturasInputDto.getId_asignaturas()){
            Estudiante_Asignatura estudiante_asignatura = estudiante_asignaturaRepositoryJpa.findById(id_asig).orElseThrow(
                    () -> new NotFoundException("MODIFICAR - Asignatura con ID *" + id_asig + "* no encontrada"));

            student.asignarAsignatura(estudiante_asignatura);
        }

        return new StudentOutputDto(student);
    }

    @Override
    public StudentOutputDto designarAsignaturas(String id, Student_AsignaturasInputDto student_asignaturasInputDto) {
        Student student = studentRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("MODIFICAR - Student con ID *" + id + "* no encontrada"));

        for(String id_asig : student_asignaturasInputDto.getId_asignaturas()){
            Estudiante_Asignatura estudiante_asignatura = estudiante_asignaturaRepositoryJpa.findById(id_asig).orElseThrow(
                    () -> new NotFoundException("MODIFICAR - Asignatura con ID *" + id_asig + "* no encontrada"));

            student.designarAsignatura(estudiante_asignatura);
        }

        return new StudentOutputDto(student);
    }

    @Override
    public void eliminar(String id){
        try {
            Student studentEncontrar = studentRepositoryJpa.findById(id).get();
            studentRepositoryJpa.deleteById(studentEncontrar.getId_student());
        }catch (NoSuchElementException k){
            throw new NotFoundException("ELIMINAR - Student con id *" + id + "* no encontrada");
        }
    }

    @Override
    public StudentOutputDto devolverPorID(String id) {
        try {
            Student studentEncontrar = studentRepositoryJpa.findById(id).get();
            return new StudentOutputDto(studentEncontrar);
        }catch (NoSuchElementException k){
            throw new NotFoundException("DEVOLVER POR ID - Student con id *" + id + "* no encontrada");
        }
    }

    @Override
    public Student_PersonaOutputDto devolverPersonaStudentPorID(String id) throws NotFoundException{
        Student studentEncontrar = studentRepositoryJpa.findById(id).orElseThrow(
                () -> new NotFoundException("DEVOLVER POR ID - Student con id *" + id + "* no encontrada"));
        return new Student_PersonaOutputDto(studentEncontrar);
    }

    @Override
    public List<StudentOutputDto> getStudents() {
        List<Student> students = studentRepositoryJpa.findAll();
        List<StudentOutputDto> studentOutputDtos = new ArrayList<>();
        for(Student s: students){
            studentOutputDtos.add(new StudentOutputDto(s));
        }
        return studentOutputDtos;
    }
}
