package com.example.BP1_ArquictecturaHexagonal.student.domain;

import com.example.BP1_ArquictecturaHexagonal.error.UnprocesableException;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.profesor.domain.Profesor;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.StudentInputDto;
import com.example.BP1_ArquictecturaHexagonal.util.StringPrefixedSequenceIdGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_seq")
    @GenericGenerator(
            name = "student_seq",
            strategy = "com.example.BP1_ArquictecturaHexagonal.util.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "Student"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%01d"),
            }
    )
    @Column(name = "ID_STUDENT")
    private String id_student;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PERSONA")
    private Persona persona;

    @Column(name = "NUMERO_HORAS_SEMANALES", nullable = false)
    private Integer num_hours_week;

    @Column(name = "COMENTARIOS")
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PROFESOR")
    private Profesor profesor;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private List<Estudiante_Asignatura> estudiante_asignaturas = new ArrayList<>();

    @Column(name = "RAMA", nullable = false) //Front, Back, FullStack
    private String branch;

    public Student(StudentInputDto studentInputDto, Persona persona, Profesor profesor){
        this.persona = persona;
        this.profesor = profesor;
        if(studentInputDto.getNum_hours_week() != null) this.num_hours_week = studentInputDto.getNum_hours_week();
            else throw new UnprocesableException("CREAR - Campo num_hours_week vacio");
        if(studentInputDto.getComment() != null) this.comment = studentInputDto.getComment();
            else throw new UnprocesableException("CREAR - Campo comment vacio");
        if(studentInputDto.getBranch() != null) this.branch = studentInputDto.getBranch();
            else throw new UnprocesableException("CREAR - Campo branch vacio");
    }

    public void setStudent(String id, StudentInputDto studentInputDto, Persona persona){
        if(studentInputDto.getId_persona() != null) this.persona = persona;
            else throw new UnprocesableException("Imposible MODIFICAR student con ID " + id + ": campo id_persona vacio");
        if(studentInputDto.getNum_hours_week() != null) this.num_hours_week = studentInputDto.getNum_hours_week();
            else throw new UnprocesableException("Imposible MODIFICAR student con ID " + id + ": campo num_hours_week vacio");
        if(studentInputDto.getComment() != null) this.comment = studentInputDto.getComment();
            else throw new UnprocesableException("Imposible MODIFICAR student con ID " + id + ": campo comment vacio");
        if(studentInputDto.getBranch() != null) this.branch = studentInputDto.getBranch();
            else throw new UnprocesableException("Imposible MODIFICAR student con ID " + id + ": campo branch vacio");
    }

    public void asignarAsignatura(Estudiante_Asignatura estudiante_asignatura){
        this.estudiante_asignaturas.add(estudiante_asignatura);
    }

    public void designarAsignatura(Estudiante_Asignatura estudiante_asignatura){
        this.estudiante_asignaturas.remove(estudiante_asignatura);
    }
}
