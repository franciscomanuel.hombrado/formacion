package com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.infraestructure.controller.dto.input.Estudiante_AsignaturaInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.application.StudentService;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.StudentInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input.Student_AsignaturasInputDto;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output.StudentOutputDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("estudiante")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @PostMapping
    public StudentOutputDto crearStudent(@RequestBody StudentInputDto studentInputDto) throws Exception {
        return studentService.crear(studentInputDto);
    }

    @GetMapping("{id}")
    public StudentOutputDto buscarStudentID(@PathVariable String id, @DefaultValue("simple") @RequestParam(value = "outputType") String outputType){
        switch (outputType){
            case "full":
                return studentService.devolverPersonaStudentPorID(id);

            default:
                return studentService.devolverPorID(id);

        }
    }

    @PutMapping("{id}")
    public StudentOutputDto modificar(@PathVariable String id, @RequestBody StudentInputDto studentInputDto){
        return studentService.modificar(id, studentInputDto);
    }

    @PutMapping("asignarAsignaturas/{id}")
    public StudentOutputDto asignarAsignaturas(@PathVariable String id, @RequestBody Student_AsignaturasInputDto student_asignaturasInputDto){
        return studentService.asignarAsignaturas(id, student_asignaturasInputDto);
    }

    @PutMapping("designarAsignaturas/{id}")
    public StudentOutputDto designarAsignaturas(@PathVariable String id, @RequestBody Student_AsignaturasInputDto student_asignaturasInputDto){
        return studentService.designarAsignaturas(id, student_asignaturasInputDto);
    }

    @DeleteMapping("{id}")
    public void eliminar(@PathVariable String id){
        studentService.eliminar(id);
    }

    @GetMapping
    public List<StudentOutputDto> getStudents(){
        return studentService.getStudents();
    }
}
