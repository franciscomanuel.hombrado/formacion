package com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Column;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class StudentInputDto {
    private Integer id_persona;
    private Integer num_hours_week;
    private String comment;
    private String id_profesor;
    private String branch;
    //private List<String> asignaturas;
}
