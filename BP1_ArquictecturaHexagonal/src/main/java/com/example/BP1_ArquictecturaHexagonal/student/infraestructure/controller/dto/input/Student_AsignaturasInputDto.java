package com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Student_AsignaturasInputDto {
    private List<String> id_asignaturas = new ArrayList<>();
}
