package com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StudentOutputDto {
    protected String id_student;
    protected int id_persona;
    protected Integer num_hours_week;
    protected String comment;
    protected String id_profesor;
    protected String branch;
    protected List<String> asignaturas = new ArrayList<>();

    public StudentOutputDto(Student student){
        this.id_student = student.getId_student();
        this.id_persona = student.getPersona().getId_persona();
        this.num_hours_week = student.getNum_hours_week();
        this.comment = student.getComment();
        this.id_profesor = student.getProfesor().getId_profesor();
        this.branch = student.getBranch();
        for(Estudiante_Asignatura e : student.getEstudiante_asignaturas()){
            this.asignaturas.add(e.getId_asignatura());
        }
    }
}
