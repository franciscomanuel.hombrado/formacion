package com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output;

import com.example.BP1_ArquictecturaHexagonal.estudiante_asignatura.domain.Estudiante_Asignatura;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import com.example.BP1_ArquictecturaHexagonal.student.infraestructure.controller.dto.output.StudentOutputDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Student_PersonaOutputDto extends StudentOutputDto {
    //Atributos clase Persona
    private int id_persona;
    private String usuario;
    private String password;
    private String name;
    private String surname;
    private String company_email;
    private String personal_email;
    private String city;
    private Boolean active;
    private Date created_date;
    private String imagen_url;
    private Date termination_date;

    public Student_PersonaOutputDto(Student student){
        this.id_student = student.getId_student();
        this.num_hours_week = student.getNum_hours_week();
        this.comment = student.getComment();
        this.id_profesor = student.getProfesor().getId_profesor();
        this.branch = student.getBranch();
        for(Estudiante_Asignatura e : student.getEstudiante_asignaturas()){
            this.asignaturas.add(e.getId_asignatura());
        }

        this.id_persona = student.getPersona().getId_persona();
        this.usuario = student.getPersona().getUsuario();
        this.password = student.getPersona().getPassword();
        this.name = student.getPersona().getName();
        this.surname = student.getPersona().getSurname();
        this.company_email = student.getPersona().getCompany_email();
        this.personal_email = student.getPersona().getPersonal_email();
        this.city = student.getPersona().getCity();
        this.active = student.getPersona().getActive();
        this.created_date = student.getPersona().getCreated_date();
        this.imagen_url = student.getPersona().getImagen_url();
        this.termination_date = student.getPersona().getTermination_date();
    }
}
