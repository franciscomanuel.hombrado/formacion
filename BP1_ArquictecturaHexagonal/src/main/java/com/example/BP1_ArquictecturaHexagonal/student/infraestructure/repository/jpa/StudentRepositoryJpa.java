package com.example.BP1_ArquictecturaHexagonal.student.infraestructure.repository.jpa;

import com.example.BP1_ArquictecturaHexagonal.persona.domain.Persona;
import com.example.BP1_ArquictecturaHexagonal.student.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepositoryJpa extends JpaRepository<Student, String> {
    List<Student> findByPersona(Persona persona);
}
