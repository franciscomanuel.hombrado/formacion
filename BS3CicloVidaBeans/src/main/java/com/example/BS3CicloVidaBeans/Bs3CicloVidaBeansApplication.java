package com.example.BS3CicloVidaBeans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Bs3CicloVidaBeansApplication {

	public static void main(String[] args) {
		SpringApplication.run(Bs3CicloVidaBeansApplication.class, args);
		//System.out.println(args[0]);
	}

}
