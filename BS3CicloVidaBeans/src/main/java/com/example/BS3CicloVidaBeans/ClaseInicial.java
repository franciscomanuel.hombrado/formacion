package com.example.BS3CicloVidaBeans;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ClaseInicial {

    @PostConstruct
    public void ClaseInicial(){
        System.out.println("Hola desde clase inicial");
    }

    public String saluda(){return "hola";}
}
