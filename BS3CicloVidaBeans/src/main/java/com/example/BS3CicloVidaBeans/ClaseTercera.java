package com.example.BS3CicloVidaBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClaseTercera  {

    //@Autowired
    ClaseInicial clas;

    public ClaseTercera(ClaseInicial c )
    {
        clas=c;
    }
    @Bean CommandLineRunner init(){
        return args -> {
            System.out.println(clas.saluda());
            System.out.println("Soy la tercera clase");
            System.out.println(args[0]);
        };
    }
}
