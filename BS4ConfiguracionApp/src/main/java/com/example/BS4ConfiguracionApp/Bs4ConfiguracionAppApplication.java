package com.example.BS4ConfiguracionApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bs4ConfiguracionAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(Bs4ConfiguracionAppApplication.class, args);
	}

}
