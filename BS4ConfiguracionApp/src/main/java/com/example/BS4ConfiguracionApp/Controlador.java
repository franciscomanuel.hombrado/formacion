package com.example.BS4ConfiguracionApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {
    @Autowired
    VariablesService variablesService;

    @Autowired
    Variables variables;

    @GetMapping("/valores")
    public String getVariables(){
        return "valor de var1 es: " + variablesService.getVar1() + " valor de my.var2 es: " + variablesService.getVar2();
    }

    @GetMapping("/var3")
    public String getVar3(){
        return "valor de var3 es: " + variablesService.getVar3();
    }

    @GetMapping("/valores2")
    public String getVariables2(){
        //return "valor de var1 es: " + variablesService.getVar1() + " valor de my.var2 es: " + variablesService.getVar2();
        return "valor de var2 es: " + variables.var2+ " valor de my.var3 es: " + variables.var3;
    }
}
