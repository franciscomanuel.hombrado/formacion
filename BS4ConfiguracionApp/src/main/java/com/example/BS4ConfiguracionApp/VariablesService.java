package com.example.BS4ConfiguracionApp;

public interface VariablesService {
    public String getVar1();
    public void setVar1(String var1);

    public String getVar2();
    public void setVar2(String var2);

    public String getVar3();
    public void setVar3(String var3);
}
