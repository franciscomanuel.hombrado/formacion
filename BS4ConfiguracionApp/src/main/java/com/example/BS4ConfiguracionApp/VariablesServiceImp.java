package com.example.BS4ConfiguracionApp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class VariablesServiceImp implements VariablesService{
    @Value("${VAR1}")
    private String var1;
    @Value("${My.VAR2}")
    private String var2;
    @Value("${VAR3:var3 no tiene valor}")
    private String var3;

    @Override
    public String getVar1() {
        return var1;
    }

    @Override
    public void setVar1(String var1) {
       this.var1=var1;
    }

    @Override
    public String getVar2() {
        return var2;
    }

    @Override
    public void setVar2(String var2) {
        this.var2=var2;
    }

    @Override
    public String getVar3() {
        return var3;
    }

    @Override
    public void setVar3(String var3) {
        this.var3 = var3;
    }
}
