package com.example.BS4Perfiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bs4PerfilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Bs4PerfilesApplication.class, args);
	}

}
