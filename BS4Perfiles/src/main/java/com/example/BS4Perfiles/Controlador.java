package com.example.BS4Perfiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {
    @Value("${url}")
    private String url;

    @Value("${password}")
    private String password;

    @Autowired
    Configuracion configuracion;

    @Autowired
    Perfiles perfil;

    @GetMapping("/parametros")
    public String getParametros(){
        return "URL: " + url + ", PASSWORD: " + password;
    }

    @GetMapping("/miconfiguracion")
    public String getMiConfiguracion(){
        return "valor1: " + configuracion.getValor1() + ", valor2: " + configuracion.getValor2();
    }

    @GetMapping("/perfil")
    public String getPerfil(){
        return perfil.getPerfil();
    }
}
