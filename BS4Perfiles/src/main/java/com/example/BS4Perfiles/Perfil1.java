package com.example.BS4Perfiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("perfil1")
public class Perfil1 implements Perfiles{
    private String perfil1="perfil1";

    @Override
    public void miFuncion() {
        System.out.println("hola desde perfil1");
    }

    @Override
    public String getPerfil() {
        return perfil1;
    }
}
