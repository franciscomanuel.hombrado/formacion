package com.example.BS4Perfiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("perfil2")
public class Perfil2 implements Perfiles{
    private String perfil2="perfil2";

    @Override
    public void miFuncion() {
        System.out.println("hola desde perfil2");
    }

    @Override
    public String getPerfil() {
        return perfil2;
    }
}
