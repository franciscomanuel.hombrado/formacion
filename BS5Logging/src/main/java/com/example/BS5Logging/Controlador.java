package com.example.BS5Logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class Controlador {
    public Controlador(){
        for(int i = 0; i < 500; i++){
            log.error(i + " mensajes de error");
        }

    }
}
