package com.example.DB1_JPA_Hibernate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controlador {
    @Autowired
    PersonaRepositorio personaRepositorio;

    @PostMapping("/anadirPersona")
    public Persona nuevaPersona(@RequestBody Persona persona){
        if(ningunValorNulo(persona)) {
            personaRepositorio.save(persona);
            return persona;
        }else{
            return new Persona();
        }
    }

    @GetMapping("buscarID/{id}")
    public Persona buscarPersonaID(@PathVariable int id) throws  Exception{
        return personaRepositorio.findById(id).orElseThrow(() -> new Exception("Persona no encontrada"));
    }

    @GetMapping("buscarUsuario/{usuario}")
    public List<Persona> buscarPersonaUsuario(@PathVariable String usuario){
        return personaRepositorio.findByUsuario(usuario);
    }

    @PutMapping("modificarPersona/{id}")
    public Persona modificarPersona(@PathVariable int id, @RequestBody Persona persona){
        persona.setId_persona(id);
        personaRepositorio.save(persona);
        return persona;
    }

    @DeleteMapping("eliminarPersona/{id}")
    public void eliminarPersona(@PathVariable int id){
        personaRepositorio.deleteById(id);
    }

    @GetMapping("mostrarRegistros")
    public List<Persona> getRegistros(){
        return personaRepositorio.findAll();
    }

    private boolean ningunValorNulo(Persona persona){
        if(persona.getUsuario().equals(null) || persona.getUsuario().length() < 6){
            System.out.println("usuario nulo");
            return false;
        }

        if(persona.getPassword().equals(null)){
            System.out.println("password nulo");
            return false;
        }

        if(persona.getName().equals(null)){
            System.out.println("name nulo");
            return false;
        }

        if(persona.getSurname().equals(null)){
            System.out.println("apellido nulo");
            return false;
        }

        if(persona.getCompany_email().equals(null)){
            System.out.println("companyemail nulo");
            return false;
        }

        if(persona.getPersonal_email().equals(null)){
            System.out.println("personalemail nulo");
            return false;
        }

        if(persona.getCity().equals(null)){
            System.out.println("city nulo");
            return false;
        }

        if(persona.getActive().equals(null)){
            System.out.println("active nulo");
            return false;
        }

        if(persona.getCreated_date().equals(null)){
            System.out.println("createdate nulo");
            return false;
        }

        if(persona.getImagen_url().equals(null)){
            System.out.println("imagenurl nulo");
            return false;
        }

        if(persona.getTermination_date().equals(null)){
            System.out.println("getTermination_date nulo");
            return false;
        }

        return true;
    }
}
