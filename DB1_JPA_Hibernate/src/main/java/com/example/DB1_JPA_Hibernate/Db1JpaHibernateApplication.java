package com.example.DB1_JPA_Hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Db1JpaHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(Db1JpaHibernateApplication.class, args);
	}

}
