package com.example.DB1_JPA_Hibernate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
public class Persona {
    @Id
    @GeneratedValue
    private int id_persona;

    @Column(name = "USUARIO", nullable = false, length = 10)
    private String usuario;

    @Column(name = "CONTRASEÑA", nullable = false)
    private String password;

    @Column(name = "NOMBRE", nullable = false)
    private String name;

    @Column(name = "APELLIDO")
    private String surname;

    @Column(name = "EMAIL_COMPAÑIA", nullable = false)
    private String company_email;

    @Column(name = "EMAIL_PERSONAL", nullable = false)
    private String personal_email;

    @Column(name = "CIUDAD", nullable = false)
    private String city;

    @Column(name = "ACTIVO", nullable = false)
    private Boolean active;

    @Column(name = "FECHA", nullable = false)
    private Date created_date;

    @Column(name = "ULR_IMAGEN")
    private String imagen_url;

    @Column(name = "FECHA_TERMINACION")
    private Date termination_date;
}
