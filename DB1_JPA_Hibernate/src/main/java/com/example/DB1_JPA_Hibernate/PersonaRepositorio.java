package com.example.DB1_JPA_Hibernate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonaRepositorio extends JpaRepository<Persona, Integer> {
    List<Persona> findByUsuario(String usuario);

    /*
    @Query(value = "SELECT * FROM Persona p WHERE nombre = :nombre")
    List<Persona> buscaPorNombre(String nombre);
    */
}
