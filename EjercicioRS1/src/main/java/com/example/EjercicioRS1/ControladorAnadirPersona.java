package com.example.EjercicioRS1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ControladorAnadirPersona {
    @Autowired
    PersonaService personaService;

    //@PostMapping
    @RequestMapping(value="/persona", method=RequestMethod.POST)
    public Persona addPersona(@RequestBody Persona persona){
        personaService.setListaPersonas(persona);
        return persona;
    }
}
