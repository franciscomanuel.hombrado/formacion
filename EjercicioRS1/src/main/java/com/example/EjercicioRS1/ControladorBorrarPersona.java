package com.example.EjercicioRS1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorBorrarPersona {
    @Autowired
    PersonaService personaService;

    @RequestMapping(value="/persona/{id}", method= RequestMethod.DELETE)
    public void borrarPersona(@PathVariable String id){
        personaService.borrarPersona(Integer.parseInt(id));
    }
}
