package com.example.EjercicioRS1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ControladorConsultarPersona {
    @Autowired
    PersonaService personaService;

    //@GetMapping("/persona/{id}")
    @RequestMapping(value="/persona/{id}", method= RequestMethod.GET)
    public Persona consultarPersonaId(@PathVariable String id){
        return personaService.buscaPersonaId(Integer.parseInt(id));
    }

    //@GetMapping("/persona/nombre/{nombre}")
    @RequestMapping(value="/persona/nombre/{nombre}", method=RequestMethod.GET)
    public Persona consultarPersonaNombre(@PathVariable String nombre){
        return personaService.buscaPersonaNombre(nombre);
    }
}
