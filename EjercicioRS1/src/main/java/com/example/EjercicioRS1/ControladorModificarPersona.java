package com.example.EjercicioRS1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ControladorModificarPersona {
    @Autowired
    PersonaService personaService;

    @RequestMapping(value="/persona/{id}", method= RequestMethod.PUT)
    public Persona modificarPersona(@PathVariable String id, @RequestBody Persona persona){
        return personaService.modificarPersona(Integer.parseInt(id), persona);
    }
}
