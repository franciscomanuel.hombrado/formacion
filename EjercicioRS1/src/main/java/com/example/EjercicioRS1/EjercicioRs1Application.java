package com.example.EjercicioRS1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EjercicioRs1Application {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioRs1Application.class, args);
	}

}
