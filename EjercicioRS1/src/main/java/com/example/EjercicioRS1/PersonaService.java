package com.example.EjercicioRS1;

import java.util.List;

public interface PersonaService {
    public List<Persona> getListaPersonas();
    public void setListaPersonas(Persona p);
    public Persona buscaPersonaId(int id);
    public Persona buscaPersonaNombre(String nombre);
    public Persona modificarPersona(int id, Persona persona);
    public void borrarPersona(int id);
}
