package com.example.EjercicioRS1;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonaServiceImp implements PersonaService{
    List<Persona> listaPersonas = new ArrayList<>();

    @Override
    public List<Persona> getListaPersonas() {
        return this.listaPersonas;
    }

    @Override
    public void setListaPersonas(Persona p) {
        this.listaPersonas.add(p);
    }

    @Override
    public Persona buscaPersonaId(int id) {
        if(listaPersonas.isEmpty()){
            return new Persona();
        }else {
            return listaPersonas.get(id);
        }
    }

    @Override
    public Persona buscaPersonaNombre(String nombre) {
        for(Persona persona : listaPersonas){
            if(persona.getNombre().equals(nombre)){
                return persona;
            }
        }
        return new Persona();
    }

    @Override
    public Persona modificarPersona(int id, Persona persona) {
        Persona p = buscaPersonaId(id);

        if(!p.campoNulo()){
            listaPersonas.set(id, persona);
            return persona;
        }else{
            return p;
        }
    }

    @Override
    public void borrarPersona(int id) {
        listaPersonas.remove(id);
    }
}
