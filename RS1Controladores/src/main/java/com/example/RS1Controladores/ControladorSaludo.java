package com.example.RS1Controladores;

import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ControladorSaludo {
    private static final String template = "Hola, %s";
    private final AtomicLong cont = new AtomicLong();

    @GetMapping("/saludo")
    public Saludo saludo(@RequestParam(value = "nombre", defaultValue = "Mundo")String nombre){
        return new Saludo(cont.incrementAndGet(), String.format(template, nombre));
    }

    @PostMapping("/saludoPost")
    public Saludo saludoPost(@RequestBody Saludo saludo){
        return saludo;
    }

    @GetMapping("saludo/user/{id}")
    public Saludo saludoGet(@PathVariable long id){
        return new Saludo(id, "Saludos");
    }

    //@PutMapping
}