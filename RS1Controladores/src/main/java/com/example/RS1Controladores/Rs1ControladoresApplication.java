package com.example.RS1Controladores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rs1ControladoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(Rs1ControladoresApplication.class, args);
	}

}
