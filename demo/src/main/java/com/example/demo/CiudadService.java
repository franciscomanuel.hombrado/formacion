package com.example.demo;

import org.springframework.stereotype.Component;

import java.util.List;

public interface CiudadService {
    public List<Ciudad> getCiudades();
    public void setCiudades(Ciudad ciudad);
}
