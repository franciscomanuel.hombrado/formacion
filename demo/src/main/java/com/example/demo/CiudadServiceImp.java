package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class CiudadServiceImp implements CiudadService{
    List<Ciudad> listaCiudades = new ArrayList<>();

    @Override
    public List<Ciudad> getCiudades(){
        return listaCiudades;
    }

    @Override
    public void setCiudades(Ciudad ciudad){
        listaCiudades.add(ciudad);
    }
}
