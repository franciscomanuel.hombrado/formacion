package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController //Hace un new() de esta clase, y lo guarda en su entorno
public class Controlador1 {

    @Autowired //Al ver que es una interface, busca una clase que la implemente
    PersonaService personaService;

    //Equivalente a hacer un AutoWired
    /*
    public Controlador1(PersonaService personaService){
            this.personaService = personaService;
        }
     */

    @Autowired
    CiudadService ciudades;

    @GetMapping("/controlador1/addPersona")
    public Persona setPersona(@RequestHeader Map<String, String> headers){

        headers.forEach((key, value) -> {
            switch (key){
                case "nombre":
                    personaService.setNombre(value);
                    break;
                case "poblacion":
                    personaService.setPoblacion(value);
                    break;
                case "edad":
                    personaService.setEdad(Integer.parseInt(value));
                    break;
                default:
                    break;
            }
        });

        return personaService.getPersona();
    }

    @PostMapping("/controlador1/addCiudad")
    public Ciudad añadirCiudad(@RequestBody Ciudad ciudad){
        ciudades.setCiudades(ciudad);
        return ciudad;
    }
}
