package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controlador2 {
    @Autowired
    PersonaService personaService;

    @Autowired
    CiudadService ciudadService;

    @Autowired
    @Qualifier("bean1")
    Persona bean1;

    @Autowired
    @Qualifier("bean2")
    Persona bean2;

    @Autowired
    @Qualifier("bean3")
    Persona bean3;

    @GetMapping("controlador2/getPersona")
    public Persona getPersona(){
        personaService.setEdad(personaService.getEdad() * 2);
        return personaService.getPersona();
    }

    @GetMapping("controlador2/getCiudad")
    public List<Ciudad> getCiudades(){
        return ciudadService.getCiudades();
    }

    @GetMapping("controlador2/bean/{bean}")
    public Persona getPersona1(@PathVariable String bean){
        switch(bean){
            case "bean1":
                return bean1;
            case "bean2":
                return bean2;
            case "bean3":
                return bean3;
            default:
                return new Persona();
        }
    }
}
