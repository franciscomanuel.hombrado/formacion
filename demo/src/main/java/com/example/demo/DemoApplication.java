package com.example.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication //@Configuration
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CiudadService getListaCiudades(){
		CiudadService c = new CiudadServiceImp();
		return c;
	}

	@Bean
	@Qualifier("bean1")
	public Persona getPersona1(){
		Persona p = new Persona();
		p.setNombre("bean1");
		return p;
	}

	@Bean
	@Qualifier("bean2")
	public Persona getPersona2(){
		Persona p = new Persona();
		p.setNombre("bean2");
		return p;
	}

	@Bean
	@Qualifier("bean3")
	public Persona getPersona3(){
		Persona p = new Persona();
		p.setNombre("bean3");
		return p;
	}
}