package com.example.demo;


import org.springframework.web.bind.annotation.*;

@RestController
public class MiControlador {

    @GetMapping
    public String getHolaMundo(){
        return "Holaaaaaaaaaaaaaaaaa";
    }

    //localhost:8080:ab
    @GetMapping("/ab")
    public String helloWorld() {
        return "hola mundo";
    }

    @GetMapping("/user/{nombre}")
    public String hola(@PathVariable String nombre) {
        return "Hola " + nombre;
    }

    @PostMapping("/useradd")
    public Persona mandarPersona(@RequestBody Persona persona) {
        persona.setEdad(persona.getEdad() + 1);
        return persona;
    }
}