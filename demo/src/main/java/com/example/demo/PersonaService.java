package com.example.demo;

public interface PersonaService {
    public String getNombre();

    public String getPoblacion();

    public Integer getEdad();

    public void setEdad(Integer edad);

    public void setNombre(String nombre);

    public void setPoblacion(String poblacion);

    public Persona getPersona();
}
