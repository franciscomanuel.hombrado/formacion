package com.example.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service //Esta etiqueta hace un new() de la clase //@Component
public class PersonaServiceImp implements PersonaService{

    Persona persona = new Persona();

    @Override
    public String getNombre(){
        return persona.getNombre();
    }

    @Override
    public void setNombre(String nombre){
        persona.setNombre(nombre);
    }

    @Override
    public String getPoblacion(){
        return persona.getPoblacion();
    }

    @Override
    public void setPoblacion(String poblacion){
        persona.setPoblacion(poblacion);
    }

    @Override
    public Integer getEdad(){
        return persona.getEdad();
    }

    @Override
    public void setEdad(Integer edad){
        persona.setEdad(edad);
    }

    @Override
    public Persona getPersona() {
        return persona;
    }
}
