package com.example.uploadingfiles;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.uploadingfiles.storage.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class FileUploadController {

	private final StorageService storageService;

	@Autowired
	public FileUploadController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {

		model.addAttribute("files", storageService.loadAll().map(
				path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
						"serveFile", path.getFileName().toString()).build().toUri().toString())
				.collect(Collectors.toList()));

		return "uploadForm";
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}

	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes) {

		storageService.store(file);
		redirectAttributes.addFlashAttribute("message",
				"You successfully uploaded " + file.getOriginalFilename() + "!");

		return "redirect:/";
	}

	@GetMapping("download/{nombre_archivo}")
	public ResponseEntity<Resource> downloadArchivo(@PathVariable String nombre_archivo){
		Resource archivo = storageService.loadAsResource(nombre_archivo);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + archivo.getFilename() + "\"").body(archivo);
	}

	@PostMapping("/upload/{tipo}")
	public String uploadArchivo(@PathVariable String tipo, @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes){
		String filename = file.getOriginalFilename();
		String extension = Optional.ofNullable(filename).filter(f -> f.contains(".")).map(f -> f.substring(filename.lastIndexOf(".") + 1)).get();

		switch (tipo){
			case "json":
				if(extension.equals("json")){
					storageService.store(file);
					redirectAttributes.addFlashAttribute("message",
							"Subido con éxito el archivo " + filename);
					return "redirect:/";
				}

				redirectAttributes.addFlashAttribute("message", "UPLOAD - extension del archivo incorrecta: ." + extension);
				return "redirect:/";

			case "xml":
				if(extension.equals("xml")){
					storageService.store(file);
					redirectAttributes.addFlashAttribute("message",
							"Subido con éxito el archivo ");
					return "redirect:/";
				}

				redirectAttributes.addFlashAttribute("message", "UPLOAD - extension del archivo incorrecta: ." + extension);
				return "redirect:/";

			case "csv":
				if(extension.equals("csv")){
					storageService.store(file);
					redirectAttributes.addFlashAttribute("message",
							"Subido con éxito el archivo ");
					return "redirect:/";
				}

				redirectAttributes.addFlashAttribute("message", "UPLOAD - extension del archivo incorrecta: ." + extension);
				return "redirect:/";

			default:
				redirectAttributes.addFlashAttribute("message", "UPLOAD - extension del archivo no válida: ." + extension);
				return "redirect:/";
		}
	}

	@GetMapping("/setpath")
	public String setPath(@DefaultValue() @RequestParam(value = "path") String path){
		storageService.setPath(path);
		return "setPath:/";
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

}
